# Question Data

This repository contains:

- `chapter-recognition/`: Scripts for extracting question texts and timestamp information about them from Q&A Youtube Videos
- `data/`: Collections of already gathered questions- and text-data
- `app/`: A small search interface using Next.JS/React, SearchKit+GraphQL and Elasticsearch

More information can be found in the README files of the individual folders.
