# NOTICE

- `bert_tokenizer.ts`: copied from [here](https://github.com/tensorflow/tfjs-models/blob/ba8f84d901bcafed217c4f073b052d2295caad68/qna/src/bert_tokenizer.ts) (Copyright 2020 Google LLC, Apache License 2.0); see file for changes applied;
- Font used: [Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab?category=Serif) (Christian Robertson, Apache License 2.0)
