# Question Search

Link: https://sirimangalo.gitlab.io/question-data/

## Development

```
curl -L https://sirimangalo.gitlab.io/question-data/data/data.json -o public/data/data.json
curl -L https://sirimangalo.gitlab.io/question-data/data/index.json -o public/data/index.json
curl -L https://sirimangalo.gitlab.io/question-data/data/embeddings.bin -o public/data/embeddings.bin
yarn
yarn start
```

## Model

### Convert

(only needed for new models)

```
python3 convert_model.py
tensorflowjs_converter --input_format=tf_saved_model --quantize_uint8 --output_format=tfjs_graph_model ./all-MiniLM-L6-v2_256padding ./public/models/all-MiniLM-L6-v2_256padding_quint8
```
