#!/usr/bin/env bash

DATA_HASH_BEFORE=''
DATA_HASH_AFTER=''

if [[ -f "./public/data/data.json" ]]; then
  DATA_HASH_BEFORE=$(md5sum "./public/data/data.json")
fi

python3 ./create_data.py
DATA_HASH_AFTER=$(md5sum "./public/data/data.json")

if [[ "$DATA_HASH_BEFORE" != "$DATA_HASH_AFTER" ]]; then
  # skip this step if not necessary
  yarn generateEmbeddings
fi

python3 ./create_index.py
