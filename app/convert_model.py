import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from transformers import TFAutoModel, AutoConfig

input_model = "sentence-transformers/all-MiniLM-L6-v2"
output_model = './all-MiniLM-L6-v2_256padding'

def create_model(max_len, classifier_layer=True):
    # Load tiny BERT model
    config = AutoConfig.from_pretrained(input_model)
    encoder = TFAutoModel.from_pretrained(input_model, from_pt=True,config=config)

    # Setup input layer
    input_ids = layers.Input(
        shape=(max_len,), dtype=tf.int32, name="input_ids")
    token_type_ids = layers.Input(
        shape=(max_len,), dtype=tf.int32, name="token_type_ids")
    attention_mask = layers.Input(
        shape=(max_len,), dtype=tf.int32, name="attention_mask")
    bert = encoder(
        {'input_ids': input_ids, 'token_type_ids': token_type_ids, 'attention_mask':attention_mask}
    )

    # Make sure BERT weights stay the same during training
    bert.trainable = False

    # For python training we add a classification layer
    #if classifier_layer:
    #    bert = layers.Dense(1, activation="sigmoid")(bert)

    # For TFJS we just add a layer to flatten the output
    #else:
    #    bert = layers.Flatten()(bert)

    # Put model together
    model = keras.Model(
        inputs={'input_ids': input_ids, 'token_type_ids': token_type_ids, 'attention_mask':attention_mask},
        outputs=[bert],
    )
    #loss = keras.losses.BinaryCrossentropy(from_logits=False)
    #optimizer = keras.optimizers.Adam(lr=0.0001)
    #model.compile(optimizer=optimizer, loss=[loss], metrics=["accuracy"])

    return model

if __name__ == '__main__':
    keras_model = create_model(256, False)
    keras_model.save(output_model)
