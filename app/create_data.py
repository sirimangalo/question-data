import json
import hashlib
from os import path
from glob import glob
import re

def is_valid_text(text):
    return not re.match(r'^\W*(Meditation Q&A|Meditate Together|start|end|sadhu|On the topic of.{,40}|Topic of the Dhamma Talk:.*|Welcome|Welcome Sirimangalo.org)\W*$', text, re.I)

def md5(fname):
  hash_md5 = hashlib.md5()
  with open(fname, "rb") as f:
    for chunk in iter(lambda: f.read(4096), b""):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()

file_index = []
all_data = []

for file in sorted(glob('../data/qa-*/*.json')):
  with open(file) as f:
    data = json.loads(f.read())

  videoId = data['id']
  fileId = 'qa-' + videoId

  file_basename = path.basename(file)
  # file_index.append(
  #   { 'fileId': fileId, 'md5': md5(file), 'size': path.getsize(file) }
  # )

  items = []
  for chapter in data['chapters']:
    if not is_valid_text(chapter[1]):
      continue

    items.append({
      'videoId': videoId,
      'typeId': 'qa',
      'ts': chapter[0],
      'text': chapter[1],
      'date': file_basename[:10]
    })

  all_data += items

with open('./public/data/data.json', 'w') as f:
    f.write(json.dumps(all_data[::-1]))
