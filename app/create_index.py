import json
import hashlib
from os import path
from glob import glob

def md5(fname):
  hash_md5 = hashlib.md5()
  with open(fname, "rb") as f:
    for chunk in iter(lambda: f.read(4096), b""):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()

MODEL_NAME = 'all-MiniLM-L6-v2_256padding_quint8'

with open('./public/data/index.json', 'w') as f:
    file_index = {
        'dataPath': '/data/data.json',
        'dataSize': path.getsize('./public/data/data.json'),
        'dataMD5Sum': md5('./public/data/data.json'),
        'embeddingsPath': '/data/embeddings.bin',
        'embeddingsSize': path.getsize('./public/data/embeddings.bin'),
        'embeddingsMD5Sum': md5('./public/data/embeddings.bin'),
        'modelPath': '/models/' + MODEL_NAME,
        'modelSize': sum([path.getsize(x) for x in glob('./public/models/%s/*' % (MODEL_NAME))])
    }
    f.write(json.dumps(file_index))
