const CACHE_KEY = 'question-data-cache'

// set by script
// const CACHED_FILES = []

self.addEventListener('install', (event) => {
  // clear all caches
  event.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        return caches.delete(key);
      }));
    })
  );

  // populate cache
  event.waitUntil(async function() {
    const cache = await caches.open(CACHE_KEY);
    await cache.addAll([
      ...CACHED_FILES
    ]);
  }());
});

self.addEventListener('fetch', (event) => {
  event.respondWith(async function() {
    const response = await caches.match(event.request, { ignoreSearch: true });
    return response || fetch(event.request);
  }());
});
