import React, { useEffect, useState, useRef, useMemo, useCallback } from 'react';
import localForage from 'localforage'
import scrollIntoView from 'scroll-into-view-if-needed'
import './App.css';

import ResultCard from './components/ResultCard';
import Pagination from './components/Pagination';
import SearchBox from './components/SearchBox';
import DownloadModal from './components/DownloadModal';
import SettingsModal from './components/SettingsModal';
import Icon from './components/Icon';
import { BM25 } from './helpers/bm25';

import {
  getLocalIndex,
  loadDataFromLocal
} from './helpers/storage';

import { DataItem, Settings } from './interfaces'

import {
  LOCAL_SETTINGS_KEY,
  NEURAL_WEIGHTING_FACTOR,
  SETTINGS_DEFAULT
} from './constants';

import { calculateHybridScores } from './helpers/hybrid_score';

// import big libs lazy
import type { BertTokenizer } from './helpers/bert_tokenizer'
import type { Tensor } from '@tensorflow/tfjs'
import type { GraphModel } from '@tensorflow/tfjs-converter'
import type { scoreDocuments } from './helpers/sbert';
import KDTree from 'mnemonist/kd-tree';

let scoreDocumentsFunc: typeof scoreDocuments

let TEXT_DATA: DataItem[]
let TEXT_EMBEDDINGS:Tensor
let TOKENIZER: BertTokenizer
let MODEL: GraphModel
let BM25_INDEX: BM25
let EMBEDDINGS_KDTree: any

function App() {
  const [initialLoading, setInitialLoading] = useState(true)
  const [settings, setSettings] = useState<Settings>(null)
  const [downloadModalOpened, setDownloadModelOpened] = useState(false)
  const [settingsModalOpened, setSettingsModalOpened] = useState(false)
  const [search, setSearch] = useState('')
  const [loading, setLoading] = useState(false)
  const [hits, setHits] = useState([] as any[])
  const [hitsSimilarities, setHitsSimilarities] = useState([] as any[])
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [maxPage, setMaxPage] = useState(10)
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [resultsSearch, setResultsSearch] = useState('')
  const [resultsStartDate, setResultsStartDate] = useState('');
  const [resultsEndDate, setResultsEndDate] = useState('');
  const [sortAscending, setSortAscending] = useState(false);

  const loadDataForRuntime = useCallback(async () => {
    TEXT_DATA = await loadDataFromLocal()

    if (settings.useNeuralSearch) {
      await Promise.all([
        import('./helpers/sbert'),
        import('./helpers/bert_tokenizer'),
        import('./helpers/storage')
      ]).then(async ([
        { scoreDocuments: scoreFunc },
        { loadTokenizer },
        { loadModelFromLocal, loadEmbeddingsFromLocal }
      ]) => {
        scoreDocumentsFunc = scoreFunc
        TOKENIZER = await loadTokenizer()
        TEXT_EMBEDDINGS = await loadEmbeddingsFromLocal()
        const emeddingsArray = TEXT_EMBEDDINGS.arraySync() as number[][];
        EMBEDDINGS_KDTree = KDTree.from(
          emeddingsArray.map((v, i) => [i, v]),
          emeddingsArray[0].length
        )
        MODEL = await loadModelFromLocal()
      })
    }

    BM25_INDEX = new BM25(TEXT_DATA.map(({ text }) => text))

    const lowestDate = TEXT_DATA.reduce((prev, cur) => cur.dateUnix < prev[0]
      ? [cur.dateUnix, cur.date]
      : prev,
      [Infinity, '']
    )[1] as string
    const highestDate = TEXT_DATA.reduce((prev, cur) => cur.dateUnix > prev[0]
      ? [cur.dateUnix, cur.date]
      : prev,
      [0, '']
    )[1] as string

    setStartDate(lowestDate)
    setEndDate(highestDate)

    setInitialLoading(false)
    setLoading(true)
  }, [settings])

  const readyForLoadingRuntime = useMemo(
    () => (settings && !downloadModalOpened && !settingsModalOpened),
    [settings, downloadModalOpened, settingsModalOpened]
  )

  useEffect(() => {
    if (initialLoading && readyForLoadingRuntime) {
      loadDataForRuntime()
    }
  }, [initialLoading, readyForLoadingRuntime, loadDataForRuntime])

  useEffect(() => {
    const main = async () => {
      if (!downloadModalOpened) {
        // force open the modal again in case there is no data to show
        // and the download needs to happen.
        const needsDownload = !(await getLocalIndex())
        setDownloadModelOpened(needsDownload)
      }
    }

    main()
  }, [downloadModalOpened])

  useEffect(() => {
    const main = async () => {
      if (!settingsModalOpened) {
        // load potentially updated settings from local storage into the state
        // in order to be up to date after closing.
        const newSettings: Settings = (await localForage.getItem(LOCAL_SETTINGS_KEY)) || SETTINGS_DEFAULT

        if (!(
          settings &&
          newSettings.useNeuralSearch === settings.useNeuralSearch &&
          newSettings.showScore === settings.showScore &&
          newSettings.autoStopPlaying === settings.autoStopPlaying &&
          newSettings.highlightWords === settings.highlightWords
        )) {
          setSettings(newSettings)
          setInitialLoading(true)
        }
      }
    }

    main()
  }, [settingsModalOpened, settings])

  const applyDateRangeFilter = useCallback((indices: number[], scores: number[]) => {
    const startDateDate = new Date(startDate)
    const endDateDate = new Date(endDate)

    if (
      Number.isNaN(startDateDate.getTime()) ||
      Number.isNaN(endDateDate.getTime()) ||
      startDateDate.getTime() >= endDateDate.getTime()
    ) {
      return [indices, scores]
    }

    const newHits = []
    const newScores = []

    // remove items not in date range
    for (let i = 0; i < indices.length; i++) {
      if (
        TEXT_DATA[indices[i]].dateUnix >= startDateDate.getTime() &&
        TEXT_DATA[indices[i]].dateUnix <= endDateDate.getTime()
      ) {
        newHits.push(indices[i])
        newScores.push(scores[i])
      }
    }

    return [newHits, newScores]
  }, [startDate, endDate])

  const runSearch = useCallback(async () => {
    try {
      let indices: number[], values: number[]

      if (!search) {
        indices = new Array(TEXT_DATA.length).fill(0).map((v, i) => sortAscending
          ? TEXT_DATA.length - i - 1
          : i
        )
        values = new Array(TEXT_DATA.length).fill(0)
      } else {
        const bm25Scores = BM25_INDEX.search(search)

        if (settings.useNeuralSearch) {
          const sbertScores = await scoreDocumentsFunc(
            search,
            MODEL,
            TOKENIZER,
            TEXT_EMBEDDINGS,
            EMBEDDINGS_KDTree
          )

          const hybridScores = calculateHybridScores(
            bm25Scores,
            sbertScores,
            NEURAL_WEIGHTING_FACTOR
          )

          ; [indices, values] = hybridScores
        } else {
          ; [indices, values] = bm25Scores
        }
      }

      ; [indices, values] = applyDateRangeFilter(indices, values)

      if (settings.useNeuralSearch) {
        values = values.map(v => Math.round((v / (1 + NEURAL_WEIGHTING_FACTOR)) * 100))
      } else {
        const maxValue = Math.max(...values)
        values = values.map(v => Math.round((v / maxValue) * 100))
      }

      setHits(indices)
      setHitsSimilarities(values)
      setMaxPage(Math.floor((indices.length || 0) / pageSize))
      setResultsSearch(search)
      setResultsStartDate(startDate)
      setResultsEndDate(endDate)
      setLoading(false)
      setPage(1)
    } catch(err) {
      console.error(err)
      alert('Error happened')
      setLoading(false)
    }
  }, [search, settings, startDate, endDate, pageSize, sortAscending, applyDateRangeFilter])

  useEffect(() => {
    if (loading && !initialLoading) {
      runSearch()
    }
  }, [initialLoading, loading, runSearch])

  useEffect(() => {
    if (initialLoading || (resultsEndDate === endDate && resultsStartDate === startDate)) return
    setLoading(true)
  }, [initialLoading, startDate, endDate, resultsStartDate, resultsEndDate])

  useEffect(() => {
    if (initialLoading || search === resultsSearch) return
    setLoading(true)
  }, [initialLoading, search, resultsSearch])

  useEffect(() => {
    if (initialLoading || resultsSearch) return
    setLoading(true)
  }, [initialLoading, sortAscending, resultsSearch])

  const resultsTop: any = useRef()
  useEffect(() => {
    if (!resultsTop || !resultsTop.current || !resultsTop.current) return
    scrollIntoView(resultsTop.current, { behavior: 'smooth' })
  }, [page])

  const handleDownloadModalClose = useCallback((reload = false) => {
    setDownloadModelOpened(false)
    if (reload) {
      setInitialLoading(true)
    }
  }, [])

  if (downloadModalOpened) {
    return <DownloadModal onClose={handleDownloadModalClose} />
  }

  if (settingsModalOpened) {
    return <SettingsModal onClose={() => setSettingsModalOpened(false)} />
  }

  return (
    <div className="App">
      <div className="columns is-desktop m-0">
        <div className="column" />
        <div className="column is-half-desktop">
          {initialLoading
            ? <>
              <progress className="progress is-medium is-info mt-6" max="100"></progress>
            </>
            : <>
              <div className='header'>
                <h1 className='title is-3 mb-3'>
                  <Icon icon='search' size='22px' color='#28464B' className='mr-2' />
                  Question
                  Search
                </h1>
              </div>

              <SearchBox
                initialSearch={search}
                resultsSearch={resultsSearch}
                loading={loading}
                startDate={startDate}
                endDate={endDate}
                sortAscending={sortAscending}
                onStartDateChange={(val) => setStartDate(val)}
                onEndDateChange={(val) => setEndDate(val)}
                onSubmit={(val) => setSearch(val)}
                onShowSettings={() => setSettingsModalOpened(true)}
                onShowUpdates={() => setDownloadModelOpened(true)}
                onSortOrderChange={(asc) => setSortAscending(asc)}
              />

              <span ref={resultsTop} />

              <Pagination
                page={page}
                maxPage={maxPage}
                onChange={(newPage) => setPage(newPage)}
              />

              <p className='m-2'>
                Showing <i>{hits.length}</i>
                {!resultsSearch
                  ? <> questions</>
                  : <> results for "<b>{resultsSearch}</b>"</>}
              </p>

              {hits && hits.slice((page-1)*pageSize, page*pageSize).map((hit, i) => <ResultCard
                item={TEXT_DATA[hit]}
                search={search}
                score={(hitsSimilarities[((page-1) * pageSize) + i] || 0)}
                showScore={settings.showScore}
                highlightWords={settings.highlightWords}
                autoStopPlaying={settings.autoStopPlaying}
                key={`${hit}-${TEXT_DATA[hit]['link']}`}
              />)}

              <Pagination
                page={page}
                maxPage={maxPage}
                onChange={(newPage) => setPage(newPage)}
              />
            </>
          }
        </div>
        <div className="column" />
      </div>

      <footer className="footer">
        <div className="content has-text-centered">
          <p>
            <a href="https://gitlab.com/sirimangalo/question-data/-/wikis/privacy-policy" target='_blank' rel='noreferrer nofollow'>Privacy Policy <Icon icon='external' size='15px' color='#485fc7' /></a>
            {' | '}
            <a href="https://gitlab.com/sirimangalo/question-data" target='_blank' rel='noreferrer nofollow'>Source Code <Icon icon='external' size='15px' color='#485fc7' /></a>
          </p>
        </div>
      </footer>
    </div>
  );
}

export default App;
