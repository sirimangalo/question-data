import React, { useEffect, useState } from 'react';

interface IconProps extends React.HTMLAttributes<HTMLSpanElement> {
  icon:string;
  color?:string;
  size?:string;
  style?: object;
}
function Icon({
  icon,
  color,
  size,
  style,
  ...additionalProps
}: IconProps) {
  if (!icon) {
    return
  }

  const iconsStyle = {
    ...style,
    mask: `url(${process.env.PUBLIC_URL}/icons/${icon}.svg) no-repeat center`,
    WebkitMask: `url(${process.env.PUBLIC_URL}/icons/${icon}.svg) no-repeat center`,
    maskSize: 'contain',
    WebkitMaskSize: 'contain',
    backgroundColor: color || '#000',
    minWidth: size || '20px',
    minHeight: size || '20px',
    display: 'inline-block'
  }

  return (<span style={iconsStyle} {...additionalProps} />)
}

export default Icon
