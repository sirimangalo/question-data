import React, { useEffect, useState } from 'react';
import Icon from './Icon'

function ResultCard ({
  page, maxPage, onChange
}: { page: number, maxPage: number, onChange: (newPage: number) => any }) {

  if (!page || maxPage < page) return null

  return (
    <nav className="block pagination is-flex" role="navigation" aria-label="pagination">
      <button
      style={{ order: 1 }}
      className="button is-info"
      disabled={page <= 1}
      onClick={() => onChange(page - 1)}>
        <Icon icon='left_arrow' color='#fff' />
      </button>
      <span style={{ order: 2, flex: '1 1 auto' }}>
       Page <b>{page}</b> of <b>{maxPage}</b>
      </span>
      <button
      style={{ order: 3 }}
      className="button is-info"
      disabled={page >= maxPage}
      onClick={() => onChange(page + 1)}>
        <Icon icon='right_arrow' color='#fff' />
      </button>
    </nav>
  )
}

export default ResultCard
