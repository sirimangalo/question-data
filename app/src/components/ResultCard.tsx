import React, { useEffect, useState, useMemo } from 'react';
import Icon from './Icon'
import { DataItem } from '../interfaces'
import { PODCAST_MIRROR_URL } from '../constants';
import { preprocessText } from '../helpers/preprocess'
import { highlight } from '../helpers/highlight'

interface ResultCardProps {
  item: DataItem;
  score: number;
  showScore: boolean;
  autoStopPlaying: boolean;
  highlightWords: boolean;
  search: string;
}

function ResultCard ({ item, score, showScore, autoStopPlaying, search, highlightWords }: ResultCardProps) {
  const date = new Date(Date.parse(item.date))
  const dateFormatted = date.toLocaleDateString("en-US", {
    year: 'numeric', month: 'long', day: 'numeric'
  })
  const ytLink = `https://youtu.be/${item.videoId}?t=${item.ts}`
  const audioLink = `${PODCAST_MIRROR_URL}/${item.videoId}.mp3#t=${item.ts}${autoStopPlaying && item.tsEnd ? ',' + item.tsEnd : ''}`
  const scoreFormatted = Math.round(score)

  const text = useMemo(() => highlightWords
    ? highlight(search, item.text)
    : item.text, 
    [item.text, search, highlightWords]
  )

  useEffect(() => {
    // hide player on search change
    setPlayAudio(false)
  }, [showScore])

  const [playAudio, setPlayAudio] = useState(false)
  const [loadingAudio, setLoadingAudio] = useState(false)

  return (
    <div className="card block result-card">
      <div className="card-content">
        <div className="content">
          <div className='result-text'>
            <p>{text}</p>
            <div className='is-flex is-align-items-center'>
              <button
                className={`button ${loadingAudio ? 'is-loading' : ''} ${playAudio && !loadingAudio ? 'is-danger' : ''}`}
                onClick={() => {
                  setLoadingAudio(!playAudio)
                  setPlayAudio(!playAudio)
                }}
              >
                {!loadingAudio &&
                  <Icon
                    icon={playAudio ? 'square' : 'play'}
                    size='15px'
                    color={!playAudio ? '#000' : '#fff'}
                  />}
              </button>
              {
                playAudio
                  && <audio
                  src={audioLink}
                  autoPlay={true}
                  controls={false}
                  onLoadedData={() => setLoadingAudio(false)}
                  onPause={() => {
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                  onEnded={() => {
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                  onError={() => {
                    alert('Could not load audio')
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                />
              }
              <a className='button ml-2' href={ytLink} target='_blank' rel='noreferrer nofollow'>
                <Icon icon='external' size='15px' color='#000' />
              </a>
            </div>
            <div className='result-info mt-3'>
              <time dateTime={item.date}>{dateFormatted}</time>
              <span style={{ flex: '1 auto' }}></span>
              {showScore && <span className='result-score'>score: {scoreFormatted}</span>}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ResultCard
