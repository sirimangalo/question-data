import { Settings } from "./interfaces"

export const LOCAL_MODEL_URL = 'indexeddb://tf-model'
export const LOCAL_EMBEDDINGS_KEY = 'embeddings'
export const LOCAL_DATA_KEY = 'data'
export const LOCAL_INDEX_KEY = 'local-index'

export const LOCAL_SETTINGS_KEY = 'settings'

export const SEARCH_ALGORITHMS = {
  VECTOR: 'vector',
  BM25: 'bm25'
}

export const PODCAST_MIRROR_URL = 'https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com/podcast-mirror'
export const SETTINGS_DEFAULT: Settings = {
  showScore: false,
  autoStopPlaying: true,
  useNeuralSearch: false,
  highlightWords: true
}

const REMOTE_MODEL_URL = process.env.PUBLIC_URL + 'model-test6/model.json'
const REMOTE_MODEL_IS_TFHUB = false
const MODEL_VERSION_KEY = 'model-version'

export const EMBEDDING_SIZE = 384
const REMOTE_EMBEDDINGS_URL = process.env.PUBLIC_URL + 'data/text-embeddings.bin'

const REMOTE_TEXTS_URL = process.env.PUBLIC_URL + 'data/text-data.json'

const DATA_VERSION_KEY = 'data-version'
const VERSION_URL = process.env.PUBLIC_URL + 'data/version.json'

const MIN_SIMILARITY_THRESHOLD = 0.1

export const MAX_FILE_COUNT_DOWNLOAD = 40

export const NEURAL_WEIGHTING_FACTOR = 2

export {
  REMOTE_MODEL_IS_TFHUB,
  REMOTE_MODEL_URL,
  REMOTE_TEXTS_URL,
  REMOTE_EMBEDDINGS_URL,
  DATA_VERSION_KEY,
  MODEL_VERSION_KEY,
  VERSION_URL,
  MIN_SIMILARITY_THRESHOLD
}
