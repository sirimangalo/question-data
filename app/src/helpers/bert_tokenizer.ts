/**
 * @license
 * Copyright 2020 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */

/**
 * CHANGES MADE:
 *   - Removed Trier data structure and replaced by simple map
 *   - Customized loading method for own vocabulary
 *   - Fixed Bug in Tokenizer Algorithm
 *   - Small restructuring of method names
 *   - Added custom options like padding, different return data structure
 */

import * as tf from '@tensorflow/tfjs-core';
import axios from 'axios';
import { getLocalIndex } from './storage';

const SEPERATOR = '##';
export const UNK_INDEX = 100;
export const CLS_INDEX = 101;
export const CLS_TOKEN = '[CLS]';
export const SEP_INDEX = 102;
export const SEP_TOKEN = '[SEP]';
export const NFKC_TOKEN = 'NFKC';
export const VOCAB_BASE =
    'https://tfhub.dev/tensorflow/tfjs-model/mobilebert/1/';
export const VOCAB_URL = process.env.PUBLIC_URL + 'processed_vocab.json';

function isWhitespace(ch: string): boolean {
  return /\s/.test(ch);
}

function isInvalid(ch: string): boolean {
  return (ch.charCodeAt(0) === 0 || ch.charCodeAt(0) === 0xfffd);
}

const punctuations = '[~`!@#$%^&*(){}[];:"\'<,.>?/\\|-_+=';

/** To judge whether it's a punctuation. */
function isPunctuation(ch: string): boolean {
  return punctuations.indexOf(ch) !== -1;
}

export interface Token {
  text: string;
  index: number;
}


export class BertTokenizer {
  private vocab: object = {};

  /**
  * Load the vacabulary file and initialize the Trie for lookup.
  */
  async load(customVocab: string = '') {
    let vocabText = customVocab

    if (!vocabText) {
      const localIndex = await getLocalIndex()
      const vocabUrl = process.env.PUBLIC_URL + localIndex.modelPath + '/vocab.txt'
      const { data } = await axios.get(vocabUrl)
      vocabText = data
    }

    const vocabItems = vocabText.split('\n')

    for (let vocabIndex = 0; vocabIndex < vocabItems.length; vocabIndex++) {
      const word = vocabItems[vocabIndex];
      this.vocab[word] = vocabIndex
    }
  }

  processInput(text: string): Token[] {
    const charOriginalIndex: number[] = [];
    const cleanedText = this.cleanText(text, charOriginalIndex);
    const origTokens = cleanedText.split(' ');

    let charCount = 0;
    const tokens = origTokens.map((token) => {
      token = token.toLowerCase();
      const tokens = this.runSplitOnPunc(token, charCount, charOriginalIndex);
      charCount += token.length + 1;
      return tokens;
    });

    let flattenTokens: Token[] = [];
    for (let index = 0; index < tokens.length; index++) {
      flattenTokens = flattenTokens.concat(tokens[index]);
    }
    return flattenTokens;
  }

  /* Performs invalid character removal and whitespace cleanup on text. */
  private cleanText(text: string, charOriginalIndex: number[]): string {
    const stringBuilder: string[] = [];
    let originalCharIndex = 0, newCharIndex = 0;
    for (const ch of text) {
      // Skip the characters that cannot be used.
      if (isInvalid(ch)) {
        originalCharIndex += ch.length;
        continue;
      }
      if (isWhitespace(ch)) {
        if (stringBuilder.length > 0 &&
            stringBuilder[stringBuilder.length - 1] !== ' ') {
          stringBuilder.push(' ');
          charOriginalIndex[newCharIndex] = originalCharIndex;
          originalCharIndex += ch.length;
        } else {
          originalCharIndex += ch.length;
          continue;
        }
      } else {
        stringBuilder.push(ch);
        charOriginalIndex[newCharIndex] = originalCharIndex;
        originalCharIndex += ch.length;
      }
      newCharIndex++;
    }
    return stringBuilder.join('');
  }

  /* Splits punctuation on a piece of text. */
  private runSplitOnPunc(
      text: string, count: number,
      charOriginalIndex: number[]): Token[] {
    const tokens: Token[] = [];
    let startNewWord = true;
    for (const ch of text) {
      if (isPunctuation(ch)) {
        tokens.push({text: ch, index: charOriginalIndex[count]});
        count += ch.length;
        startNewWord = true;
      } else {
        if (startNewWord) {
          tokens.push({text: '', index: charOriginalIndex[count]});
          startNewWord = false;
        }
        tokens[tokens.length - 1].text += ch;
        count += ch.length;
      }
    }
    return tokens;
  }

  /**
  * Generate tokens for the given vocalbuary.
  * @param text text to be tokenized.
  */
  private wordPieceTokenize(text: string): number[] {
    // Source:
    // https://github.com/google-research/bert/blob/88a817c37f788702a363ff935fd173b6dc6ac0d6/tokenization.py#L311

    let outputTokens: number[] = [];

    const words = this.processInput(text);
    words.forEach(word => {
      if (word.text !== CLS_TOKEN && word.text !== SEP_TOKEN) {
        word.text = `${word.text.normalize(NFKC_TOKEN)}`;
      }
    });

    for (let i = 0; i < words.length; i++) {
      const chars = [];
      for (const symbol of words[i].text) {
        chars.push(symbol);
      }

      let isUnknown = false;
      let start = 0;
      const subTokens: number[] = [];

      const charsLength = chars.length;

      while (start < charsLength) {
        let end = charsLength;
        let currIndex;

        while (start < end) {
          let substr = chars.slice(start, end).join('');

          if (start > 0) {
            substr = `${SEPERATOR}${substr}`
          }

          if (typeof this.vocab[substr] !== 'undefined') {
            currIndex = this.vocab[substr];
            break;
          }

          end = end - 1;
        }

        if (currIndex == null) {
          isUnknown = true;
          break;
        }

        subTokens.push(currIndex);
        start = end;
      }

      if (isUnknown) {
        outputTokens.push(UNK_INDEX);
      } else {
        outputTokens = outputTokens.concat(subTokens);
      }
    }

    return outputTokens;
  }

  public tokenize(
    text: string,
    options = {
      padToLength: 256
    }
  ) {
    const { padToLength } = options
    let inputIdsUnpadded = [
      CLS_INDEX,
      ...this.wordPieceTokenize(text),
      SEP_INDEX
    ]

    if (inputIdsUnpadded.length > padToLength) {
      console.warn(`This text is too long and thus trimmed in tokenization: ${text.slice(0, 40)}...`)
      inputIdsUnpadded = inputIdsUnpadded.slice(0, padToLength) 
    }

    return {
      'input_ids': tf.tensor([[
         ...inputIdsUnpadded,  ...new Array(padToLength - inputIdsUnpadded.length).fill(0) ]], null, 'int32'),
      'token_type_ids': tf.tensor([[...new Array(padToLength).fill(0)]], null, 'int32'),
      'attention_mask': tf.tensor([[
        ...new Array(inputIdsUnpadded.length).fill(1),
        ...new Array(padToLength - inputIdsUnpadded.length).fill(0)
      ]], null, 'int32')
    }
  }
}

export async function loadTokenizer(): Promise<BertTokenizer> {
  const tokenizer = new BertTokenizer();
  await tokenizer.load();
  return tokenizer;
}
