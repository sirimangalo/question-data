import { preprocessText } from './preprocess'

class BM25 {
  private numOfDocs = 0
  private wordToDocs = {}
  private docLengths = {}
  private avgDocLength = 0
  private k1 = 2.0
  private b = 0.75

  constructor(texts: string[]) {
    texts.forEach((text, i) => {
      const tokens = preprocessText(text)
      this.docLengths[i] = tokens.length
      this.avgDocLength += tokens.length
      tokens.forEach(token => {
        if (typeof this.wordToDocs[token] === 'undefined') {
          this.wordToDocs[token] = []
        }
        this.wordToDocs[token].push(i)
      })
    })

    this.numOfDocs = texts.length
    this.avgDocLength /= texts.length

    Object.entries(this.wordToDocs).forEach(([key, docIds]) => {
      const freqMap = {};
      (docIds as any[]).forEach((docId) => {
        freqMap[docId] = (freqMap[docId] || 0) + 1
      })
      this.wordToDocs[key] = Object.entries(freqMap).map(([docId, freq]) => [Number(docId), freq])
    })
  }

  search(query: string): [number[], number[]] {
    // http://ipl.cs.aueb.gr/stougiannis/bm25.html

    const queryTokens = preprocessText(query)

    if (queryTokens.length === 0) return [[],[]]

    const docScores = {}

    queryTokens.forEach(token => {
      if (!this.wordToDocs[token]) {
        return
      }

      this.wordToDocs[token].forEach(([docId, freq]) => {
        const idf = Math.log(
          (this.numOfDocs - this.wordToDocs[token].length + 0.5) /
          (this.wordToDocs[token].length + 0.5)
        )
        const score = idf * (
          (freq * (this.k1 + 1)) /
          (freq + this.k1 * (1 - this.b + this.b * (this.docLengths[docId] / this.avgDocLength)))
        )
        docScores[docId] = (docScores[docId] || 0) + score
      })
    })

    const ranked = Object.entries(docScores).sort((a: any, b: any) => b[1] - a[1])
    const indices = ranked.map(([i]) => Number(i))
    const scores = ranked.map(([_, score]) => score)

    return [indices, scores as number[]]
  }
}

export { BM25 }
