import React from 'react'
import * as stemmer from 'stemmer'
import { ENGLISH_STOPWORDS, preprocessText, REGEXP_PATTERN_PUNCT, stripPunctuation } from './preprocess'

export const highlight = (query: string, text: string) => {
  if (!query || !text) {
    return React.createElement(React.Fragment, {}, [text])
  }

  let wordBoundaryIndices: number[] = [...text.matchAll(/\s/g), ...text.matchAll(REGEXP_PATTERN_PUNCT)].map(({index}) => index)
  wordBoundaryIndices = [-1, ...wordBoundaryIndices, text.length].sort((a, b) => a - b)

  const tokens = []
  for (let i = 1; i < wordBoundaryIndices.length; i++) {
    if (i > 1) {
      tokens.push(text.slice(wordBoundaryIndices[i-1], wordBoundaryIndices[i-1] + 1))
    }

    tokens.push(text.slice(wordBoundaryIndices[i-1] + 1, wordBoundaryIndices[i]))
  }

  const queryTokens = preprocessText(query)

  const highlighted = tokens.map((token, i) => {
    if (token.length <= 1) return false
    const preprocessedToken = stemmer.stemmer(stripPunctuation(token.toLowerCase().trim())).trim()
    return preprocessedToken && !ENGLISH_STOPWORDS.includes(preprocessedToken)
      && queryTokens.includes(preprocessedToken)
  })

  const children = []
  highlighted.forEach((isHl, i) => {
    if (isHl) {
      children.push(<b>{tokens[i]}</b>) 
    } else {
      children.push(tokens[i])
    }
  })

  return React.createElement(
    React.Fragment, 
    {}, 
    children
  )
}
