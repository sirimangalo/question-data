export const calculateHybridScores = (
  bm25Scores: [number[], number[]],
  sbertScores: [number[], number[]],
  lambda: number = 1.67,
  normalizeVectorScore: boolean = false
): [number[], number[]] => {
  const bm25MaxScore = Math.max(...bm25Scores[1])
  const sbertMaxScore = Math.max(...sbertScores[1])

  const bm25Ids = bm25Scores[0].reduce((prev, docId, i) => ({ 
    ...prev, 
    [docId]: bm25Scores[1][i] / bm25MaxScore  
  }), {})

  const sbertIds = sbertScores[0].reduce((prev, docId, i) => ({ 
    ...prev, 
    [docId]: lambda * (normalizeVectorScore 
      ? (sbertScores[1][i] / sbertMaxScore) 
      : sbertScores[1][i])
  }), {})

  const hybridScores = []

  Array.from(new Set([
    ...Object.keys(sbertIds).map(i => Number(i)),
    ...Object.keys(bm25Ids).map(i => Number(i))
  ])).forEach(key => {
    hybridScores.push([
      key,
      (bm25Ids[key] || 0) + (sbertIds[key] || 0)
    ])
  })

  hybridScores.sort((a, b) => b[1] - a[1])
  
  return [
    hybridScores.map(x => x[0]),
    hybridScores.map(x => x[1])
  ]
}
