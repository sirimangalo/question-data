import { registerKernel } from '@tensorflow/tfjs'
import * as stemmer from 'stemmer'

export const ENGLISH_STOPWORDS = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"]

export const REGEXP_PATTERN_PUNCT = new RegExp("['!\"“”#$%&\\'()\*+,\-\.\/:;<=>?@\[\\\]\^_`{|}~']", 'g')

export const tokenize = (s: string): string[] => s.match(/\w+/g)

export const stripPunctuation = (s: string): string => s.replace(REGEXP_PATTERN_PUNCT, "")

export const preprocessText = (sentence: string): string[] => tokenize(sentence.toLowerCase())
    .map(s => stripPunctuation(s))
    .map(s => stemmer.stemmer(s))
    .filter(s => s)
    .filter(s => !ENGLISH_STOPWORDS.includes(s))
