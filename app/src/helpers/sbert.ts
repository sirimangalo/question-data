import '@tensorflow/tfjs-backend-cpu';
import '@tensorflow/tfjs-backend-webgl';
import * as tf from '@tensorflow/tfjs-core';
import { GraphModel } from '@tensorflow/tfjs';
import { BertTokenizer } from './bert_tokenizer';
import { meanPooling } from './pooling';

const generateEmbedding = (
  sbertModel: GraphModel,
  tokenizer: BertTokenizer,
  text: string
) => {
  return tf.tidy(() => {
    const tokens = tokenizer.tokenize(text)
    const modelOut = sbertModel.predict(tokens)
    let queryEmbedding = meanPooling(modelOut[0], tokens['attention_mask'])
    return tf.div(queryEmbedding, tf.norm(queryEmbedding, 2))
  })
}

const scoreDocuments = async (
  queryString: string,
  model: GraphModel,
  tokenizer: BertTokenizer,
  documentEmbeddings: tf.Tensor,
  kdTree: any,
  topK = 50
): Promise<[number[], number[]]> => {

  const { indices, values } = tf.tidy('calculate similarity matrix', () => {
    const queryEmbedding = generateEmbedding(model, tokenizer, queryString);
    
    const nearestIndices: number[] = kdTree.kNearestNeighbors(topK, queryEmbedding.arraySync()[0]);
    const nearestEmbeddings = documentEmbeddings.gather(tf.tensor1d(nearestIndices, 'int32'));

    // this looks like dot-product only, but is actually cosine similarity since the vectors are normalized to 1
    const cosineSimilarityMatrix = tf.matMul(nearestEmbeddings, queryEmbedding, false, true);
    const { values } = tf.topk(tf.reshape(cosineSimilarityMatrix, [-1]), nearestEmbeddings.shape[0], true);

    return { indices: nearestIndices, values };
  })

  const valuesArray = await values.array() as number[];

  return [
    indices || [],
    valuesArray || []
  ]
}

export { generateEmbedding, scoreDocuments }
