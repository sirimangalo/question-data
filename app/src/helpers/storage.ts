import type { Tensor } from '@tensorflow/tfjs'
import type { GraphModel } from '@tensorflow/tfjs-converter'

import axios from 'axios'
import localForage from 'localforage'

import {
  LOCAL_EMBEDDINGS_KEY,
  LOCAL_MODEL_URL,
  LOCAL_DATA_KEY,
  LOCAL_INDEX_KEY,
  EMBEDDING_SIZE
} from '../constants'

import { DataIndex, DataItem, DownloadStrategy } from '../interfaces';


export const getLocalIndex = async (): Promise<DataIndex> => {
  return localForage.getItem(LOCAL_INDEX_KEY)
}

const setLocalIndex = async (indexData: DataIndex): Promise<void> => {
  await localForage.setItem(LOCAL_INDEX_KEY, indexData)
}

const getRemoteIndex = async (): Promise<DataIndex> => {
  const { data } = await axios.get(`${process.env.PUBLIC_URL}/data/index.json`)
  return data
}

export const getUpdateStrategy = async (): Promise<DownloadStrategy> => {
  const localIndex: DataIndex = await getLocalIndex()
  const remoteIndex: DataIndex = await getRemoteIndex()

  const strategy: DownloadStrategy = {
    queue: [],
    queueSize: 0,
    newIndex: remoteIndex
  }

  if (!localIndex?.dataMD5Sum || localIndex.dataMD5Sum !== remoteIndex.dataMD5Sum) {
    strategy.queue.push({
      filePath: process.env.PUBLIC_URL + remoteIndex.dataPath,
      fileSize: remoteIndex.dataSize,
      md5Sum: remoteIndex.dataMD5Sum,
      localKey: LOCAL_DATA_KEY,
      type: 'file'
    })
    strategy.queueSize += remoteIndex.dataSize
  }

  if (!localIndex?.embeddingsMD5Sum || localIndex.embeddingsMD5Sum !== remoteIndex.embeddingsMD5Sum) {
    strategy.queue.push({
      filePath: process.env.PUBLIC_URL + remoteIndex.embeddingsPath,
      fileSize: remoteIndex.embeddingsSize,
      md5Sum: remoteIndex.embeddingsMD5Sum,
      localKey: LOCAL_EMBEDDINGS_KEY,
      type: 'file'
    })
    strategy.queueSize += remoteIndex.embeddingsSize
  }

  if (!localIndex?.modelPath || localIndex.modelPath !== remoteIndex.modelPath) {
    strategy.queue.push({
      filePath: process.env.PUBLIC_URL + remoteIndex.modelPath + '/model.json',
      fileSize: remoteIndex.modelSize,
      localKey: LOCAL_MODEL_URL,
      md5Sum: '',
      type: 'model'
    })
    strategy.queueSize += remoteIndex.modelSize
  }

  return strategy
}

export const downloadAndApplyUpdates = async (strategy: DownloadStrategy): Promise<void> => {
  if (strategy.queueSize === 0) {
    return
  }

  let backup = {}
  let backupModel = null

  try {
    for (const item of strategy.queue) {
      if (item.type === 'file') {
        const { data } = await axios.get(
          item.filePath,
          item.filePath.endsWith('.bin') ? { responseType: 'arraybuffer' } : undefined
        )
        backup[item.localKey] = data
        await localForage.setItem(item.localKey, data)
      } else if (item.type === 'model') {
        try {
          backupModel = await loadModelFromLocal()
        } catch (err) {
        }

        await import('@tensorflow/tfjs-backend-cpu')
        const tfconv = await import('@tensorflow/tfjs-converter')
        const model = await tfconv.loadGraphModel(
          item.filePath,
          { fromTFHub: false }
        )

        await model.save(LOCAL_MODEL_URL)
      }
    }

    await setLocalIndex(strategy.newIndex)
  } catch(err) {
    // try to restore state as before update to prevent corruptions
    for (const [key, data] of Object.entries(backup)) {
      await localForage.setItem(key, data)
    }

    if (backupModel) {
      await backupModel.save(LOCAL_MODEL_URL)
    }

    throw err
  }
}

export const loadModelFromLocal = async (): Promise<GraphModel> => {
  const tfconv = await import('@tensorflow/tfjs-converter')
  const model = await tfconv.loadGraphModel(LOCAL_MODEL_URL)
  return model
}

export const loadDataFromLocal = async (): Promise<DataItem[]> => {
  const textData: DataItem[] = (await localForage.getItem(LOCAL_DATA_KEY)) as DataItem[]
  textData.forEach((item, i) => {
    item.dateUnix = new Date(item.date).getTime()

    if ((i + 1) < textData.length &&
      textData[i+1].videoId === item.videoId &&
      textData[i+1].typeId === item.typeId
    ) {
      textData[i + 1].tsEnd = item.ts
    }
  })
  return textData
}

export const loadEmbeddingsFromLocal = async (): Promise<Tensor> => {
  const embeddings: ArrayBuffer = (await localForage.getItem(LOCAL_EMBEDDINGS_KEY)) as ArrayBuffer
  const tf = await import('@tensorflow/tfjs')
  return tf.reshape(tf.tensor(new Float32Array(embeddings)), [-1, EMBEDDING_SIZE])
}
