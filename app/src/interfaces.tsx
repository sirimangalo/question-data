export interface DataIndex {
  dataPath: string;
  dataSize: number;
  dataMD5Sum: string;
  embeddingsPath: string;
  embeddingsSize: number;
  embeddingsMD5Sum: string;
  modelPath: string;
  modelSize: number;
}

export interface DownloadItem {
  filePath: string;
  fileSize: number;
  localKey: string;
  type: 'file' | 'model';
  md5Sum: string;
}

export interface DownloadStrategy {
  queue: DownloadItem[];
  queueSize: number;
  newIndex: DataIndex;
}

export interface DataItem {
  videoId: string;
  typeId: string;
  ts: number;
  text: string;
  date: string;
  dateUnix?: number;
  tsEnd?: number;
}

export interface Settings {
  showScore: boolean;
  autoStopPlaying: boolean;
  useNeuralSearch: boolean;
  highlightWords: boolean;
}
