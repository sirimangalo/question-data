import * as tfn from '@tensorflow/tfjs-node';

import fs from 'fs';
import * as tf from '@tensorflow/tfjs-core'
import { generateEmbedding } from '../helpers/sbert'
import { BertTokenizer } from '../helpers/bert_tokenizer'
import * as process from 'process'
import * as tfconv from '@tensorflow/tfjs-converter';

const data = JSON.parse(
  fs.readFileSync(__dirname + '/../../public/data/data.json', { encoding: 'utf-8' })
)

const MODEL_NAME = 'all-MiniLM-L6-v2_256padding_quint8'
const MODEL_PATH = __dirname + `/../../public/models/${MODEL_NAME}/`

const main = async () => {
  const tokenizer = new BertTokenizer();
  await tokenizer.load(fs.readFileSync(MODEL_PATH + 'vocab.txt', { encoding: 'utf-8' }));
  const model = await tfconv.loadGraphModel(
    tfn.io.fileSystem(MODEL_PATH + '/model.json')
  )

  const batchSize = 100
  const embeddings = tf.concat(
    new Array(Math.ceil(data.length / batchSize)).fill(0).map((_, i) =>
      tf.concat(
        data.slice(i * batchSize, (i + 1) * batchSize)
          .map(({ text }) => generateEmbedding(model, tokenizer, text))
      )
    )
  )

  embeddings.print(true)
  fs.writeFileSync(__dirname + '/../../public/data/embeddings.bin', embeddings.dataSync())
}

main()
  .then(() => process.exit())
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
