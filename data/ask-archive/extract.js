const fs = require('fs');
const Xray = require('x-ray');

const x = Xray();

x(fs.readFileSync('./book.html'), '.category', [
  {
    catgeory: '.cat-title a',
    questions: x('.question', [
      {
        title: '.question-title',
        link: '.question-title a@href',
        anchor: '.question-title a@name',
        content: '.question-content@html',
        answers: x('.answers', [
          {
            content: '.answer-content@html',
            user: '.answer-meta a'
          }
        ])
      }
    ])
  }
]).write('book.json');
