const Crawler = require('crawler');
const fs = require('fs');

const questions = [];

const cQuestion = new Crawler({
  callback: function (err, res, done) {
    if (err) {
      throw err;
    }

    const $ = res.$;
    const question = {
      title: $('h1').text().trim(),
      link: $('h1 a').attr('href'),
      text: $('.qa-post-content div').html(),
      author: $('.qa-q-view-who-data').text().trim(),
      date: $('.qa-q-view-when-data time').attr('datetime'),
      votes: $('.qa-q-view-stats .qa-netvote-count-data').text().trim(),
      views: $('.qa-view-count-data').text().trim(),
      comments: [],
      answers: [],
      tags: []
    };

    $('.qa-c-list-item ').each((i2, elComment) => {
      const comment = {
        user: $(elComment).find('.qa-c-item-who-data').text().trim(),
        date: $(elComment).find('.qa-c-item-when-data time').attr('datetime'),
        text: $(elComment).find('.qa-post-content div').html()
      };

      question.comments.push(comment);
    });

    $('.qa-a-list-item').each((i, el) => {
      const answer = {
        date: $(el).find('.qa-a-item-when time').attr('datetime'),
        user: $(el).find('.qa-user-link').text().trim(),
        text: $(el).find('.qa-post-content div').html(),
        votes: $(el).find('.qa-netvote-count-data').text().trim(),
        comments: []
      };

      $(el).find('.qa-c-list-item').each((i2, elComment) => {
        const comment = {
          user: $(elComment).find('.qa-c-item-who-data').text().trim(),
          date: $(elComment).find('.qa-c-item-when-data time').attr('datetime'),
          text: $(elComment).find('.qa-post-content div').html()
        };

        answer.comments.push(comment);
      });

      question.answers.push(answer);
    });

    $('.qa-q-view-tag-item').each((i, el) => {
      question.tags.push($(el).text())
    });

    questions.push(question);

    done();
  }
})

const cQuestions = new Crawler({
  callback: function (err, res, done) {
    if (err) {
      throw err;
    }

    const $ = res.$;
    $('.qa-q-item-title > a').each((i, el) => {
      cQuestion.queue($(el).attr('href').replace('.', 'https://ask.sirimangalo.org'));
    });

    if ($('.qa-page-next') && $('.qa-page-next').attr('href')) {
      cQuestions.queue($('.qa-page-next').attr('href').replace('.', 'https://ask.sirimangalo.org'));
    }

    done();
  }
});

cQuestions.queue('https://ask.sirimangalo.org/questions');

process.on('exit', () => {
  fs.writeFileSync('out.json', JSON.stringify(questions, null, 2), () =>{});
});