#!/usr/bin/env python3

import json
from glob import glob
from datetime import datetime, timedelta
from os import path

def parse_time(time_str: str):
    time = datetime.strptime(time_str.strip(), "%H:%M:%S" if len(time_str) in [7, 8] else "%M:%S")
    seconds = timedelta(hours=time.hour, minutes=time.minute, seconds=time.second).seconds
    assert seconds >= 0

    return seconds

def parse_file(filename: str):
    with open(filename) as f:
        lines = f.read().split('\n')

    data = []
    for line in lines:
        if not line.strip():
            continue

        time_str, text = line.split(' ', 1)
        time = parse_time(time_str)
        text = text.strip()
        data.append([time, text])

    return sorted(data, key=lambda x: x[0])

todo = glob('qa-*/*.txt')

for file in todo:
    yt_id = path.basename(file).replace('.txt', '')[11:]
    data = {
        'id': yt_id,
        'chapters': parse_file(file)
    }

    with open(file.replace('.txt', '.json'), 'w') as f:
        f.write(json.dumps(data, indent=2))
