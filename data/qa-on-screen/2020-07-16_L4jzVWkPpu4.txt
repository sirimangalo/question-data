00:00 Start
10:46 I sense greater detachment when, noticing thoughts, I repeat "thoughts" instead of "thinking", as you and the booklet teach. What is your opinion?
12:56 If I have nerve pain, and I can get rid of the pain with my mind, can I heal the nerves with my mind by meditation or mindfulness? Will the nerves still be damaged or will they eventually heal?
15:37 I have been meditating for hours and days and can't see that much of progress. What am I doing wrong? I just let everything be and be aware but idk why it's not working.
17:58 Lately my attention is either restless or dull, can't find much stability on the breathe. Therefore I put attention to the whole body to avoid aversion to practice. Good idea or do I stay on breath.
20:34 After 40 minutes I needed no more physical effort to maintain the upright position. Relaxing mentally I felt collapsing like a string-less puppet even if my physical body was immovable. what is this?
22:24 I find that in my daily life I struggle to stay mindful while people speak to me because I'll lose what they're saying to me. Will I eventually be able to focus on both given practice?
23:58 When doing sitting meditation watching the breath and then the thoughts come and I say “thinking, thinking”. How do you know for how long you say “thinking” before you go back to watching the breath?
25:02 Doesn't happiness depend on conditions? And therefore unstable? Therefore not under my own power?
25:31 How do we deal with parents that do not accept your desire to become a monk? My mom wants me to finish college before I ordain but I want to drop out and ordain.
32:28 When doing walking meditation should we see the step as more movements like stepping forward and then placing the feet down or is it okay to see the step as one movement?
34:49 When I first start meditating I have a tendency to take deeper breaths when watching my stomach but then it settles down. Do a lot of people experience this?
36:57 Why do my hands get stuck together when I’m meditating?
37:11 While meditating for an hour, is experiencing pain a natural phenomena that one has to accept?
38:38 Even when people ill-treat me, I see their suffering before seeing the detrimental effects on me and allow them to harm me. Is this behaviour in sync with dhamma?
43:57 If one is meditating on the breath but within 39-40 minutes one begins to experience pain in the legs due to numbness, etc, should we then meditate on the pain?
45:08 When meditating a pain in the top left side of my head is almost always the most prominent sensation. Should I always keep noting that continuously?
45:52 You said you find it funny when people say "mindfully" switching uncomfortable positions. If you become aware that you are unconsciously controlling the breath, shouldn't you switch to natural breath?
51:06 Since Buddha suggests not to associate with fools I tend to judge the people around me. How can we deal with the arrogance of feeling superior because we suppose we are following a higher path?
55:17 During the course of the day, when one is not doing formal meditation, should one be mindful of the activities of what you are doing or aware of the arising of the abdomen?
56:19 Do you have any experience with or know anyone with experience going against a natural circadian rhythm? I start an overnight shift soon. I will have lots of time to meditate at least!
57:29 Noting the hindrances is enough to understand suffering?
58:09 Bhante, this is my first time on live chat, but what is the name of the book you are suggesting that one read? How to Meditate Booklet: https://htm.sirimangalo.org/
1:00:57 I tend to overthink so in the practice I frequently note "thinking". So let me get this straight, the path is just mindfulness? Is it enough? What about taking a diary and reflecting and studying?
1:03:04 Are the courses offered at Wat Chom Tong identical to the ones offered by you in Canada? Is there some charge for them, and can one stay at the monastery and self practice between courses? Thank you
1:06:28 End
