00:00 Welcome
16:18 Is metta-practice worldly, and therefore not useful for ultimate insight?
17:44 How does one keep the human monkey brain from bringing them back to society?
20:35 What are your thoughts on guided meditations? My meditation teacher says to start with guided meditations, as it'll improve concentration, then to move on to unguided after a few months of daily practice.
21:41 When I meditate, can I put a pillow under my back, and lean on the wall and sit on the floor, so I avoid pain because of the pelvic tilt?
22:33 How do we know if we're becoming complacent in our meditation practice?
25:07 I find myself "anticipating" the labels rather than feeling the experience. What situation is this? They are quite automatic, without presence. Compared to awareness without labels, why do we label?
28:44 Is it better to be mindful in every moment, even though every moment is painful? And why?
29:40 How can we determine what might be "inconsequential" in meditation? Clock ticking, random sounds that my house makes, etc. Can these just be ignored from the get go?
30:53 How can a feeling of conceit be noted?
32:01 How do I build a daily meditation habit?
33:03 Do you have any practical advice for getting out of a rut where your mindfulness just isn't there and feels far away? Sometimes it feels like I have to struggle a lot before getting "over the hump".
34:46 What should I do with my negative thoughts? I notice it there and keeps repeating for a day or two.
35:24 During meditation, I struggle a bit with the notion of noting and letting go, and the realization of suffering, Impermanence and Non-Self. Should I not be returning to the breath when anything arises?
36:16 Could the practice be as a side effect beneficial for the work? In general for problem-solving? I find that 5 minutes of practice, a walk, or a shower (don't know why) has quite an impact.
37:39 I have made strides in purifying hatred and anger from my mind through seeing clearly, but fear remains a constant issue. Fear and anxiety is always too strong for noting, or anything else to help it. May I ask your advice?
40:05 Can someone join the meditation at-home course without being Buddhist?
41:39 How to see arising and passing with static physical sensations that are always there?
43:11 My job requires me to exercise. How do I stay mindful then? Is it rising/falling or running, running?
43:55 What should I do about physical symptoms of anxiety? I hear advice about focusing on the symptoms and I also have thoughts that run wild, so which should I focus on?
45:28 How does one note the dhammas? I feel that I understand the basics of noting the body, mind, and feelings, but not the dhammas.
46:14 What is the Buddhist attitude towards filial piety? My sense of responsibility towards my parents is sometimes paralyzing...
49:02 I am a student, studying for a university entrance exam. What to do for better focus, better understanding, and less stress? Also: what is the right way to do meditation for an always-excited person?
49:43 I had read a book on Buddhism and it talked about how the spiritual world is what we should focus on, and not the physical world. It has sent me in a bit of existential dread. How shall I take this?
50:55 Is there any notion of Abhidhamma useful for practice? Being inclined to study, what would in general you recommend to deepen studying as a support for the practice?
53:02 Realizations during meditation involve thought. If thought is to be noted and the mind returned to the rising and falling, how does realization develop?
55:09 I have anxiety but I'm afraid to meditate because I believe that I have attention deficit disorder. Are there other ways to practice meditation or do better with anxiety?
56:40 Please clarify the difference between “getting caught in the small detail”, and “breaking down the experience” (developing the practice means breaking down the experience & not using blanket noting terms).
1:00:14 What's this booklet? And where can I find it?
1:01:20 I would like to ask for some advice: I find that the sensations of breathing disappear. I focus on the abdomen. I feel severe anxiety. What should I do, where to focus, when the breath is gone?
1:02:52 Sadhu...
