00:00 Welcome
04:39 On the topic of Wisdom
27:17 Sometimes when I notice and note hearing, I almost automatically subsequently "open" my hearing awareness to whatever sounds are there and then note them. Is this incorrect practice? It's an old habit.
28:24 Should you focus on helping yourself through meditation instead of using your time to help others?
29:34 Feeling cold after meditation. Should I do anything?
29:58 How to deal with relations when people get unintentionally irritated/offended from your behaviour? The continuous misunderstandings with others are stressful.
33:06 In meditation, sometimes I feel like I'm trying to change things. I often catch myself trying to force equanimity, even in the tone of voice when noting in my head. I note "knowing". Any other advice?
34:04 Is it OK to note in English, even though it is not my first language, and to use my first language for words I don't know in English? I ask because I feel that English better fits the experience.
34:53 Sometimes when I am focusing on the stomach and become aware of thinking, before I can go away from the stomach to note “thinking”, I am pulled back to the stomach. Should I still note “thinking”?
36:14 Do I have to note the disliking of an object to get the benefit of becoming dispassionate about said disliking? Should I give “liking” and “disliking” some priority vs. “discomfort” / “tension”? Sometimes both seem to arise at once.
37:01 Are there any tips on dealing with aversions - especially the subtle ones - toward emotions? Even when I am trying to just accept my emotions, it always feels like it is only to get rid of it.
38:04 When hearing others speak, my inner monologue seems to "repeat" what they are saying in my head. Because of this, I am having trouble noting "hearing." How should I persevere?
39:09 How can one note disjointed, fragmented, incomplete thoughts? I always note thinking, but sometimes it feels lazy to note it as such, and seems to prompt more similar "thoughts".
40:03 If while meditating, you note clinging to something, how would this stop the clinging? You could observe the same clinging over and over again.
42:20 In my practice, I have noticed some things. The more prevalent feelings are fear, anxiety, obsession and compulsion (diagnosed with OCD). Sometimes I also notice impermanence, but only intellectually. What should I do about these feelings, aside from noting?
43:44 I noted I feel not-good if I don’t do enough meditation. What should I do? Should I let go? Maybe is this also seeing “uncontrollability”.
44:33 After almost a year straight of daily-meditation, I've found myself becoming distracted, and meditating inconsistently. Are there meditations/recommended practices to deal with this?
45:44 When someone has tinnitus - like myself - how should you note this? Hearing or thought?
46:06 For a distracted/obsessive mind, it takes quite some effort to get it to stabilize on the object. What is the relationship between restraint (of mind wandering), mindfulness and unification of mind?
47:09 How does one know when one is doing meditation "right"? And how does one know they're doing it wrong?
49:05 The more time I spend unmindful, the more difficult it is for me to be mindful, especially throughout the day. Are “unmindful" moments due to a lack of effort?
50:29 Is it a good practice to work on one of the 10 perfections while meditating? Like noting rising and falling with the intention to enhance the quality of persistence in oneself.
51:17 After years of hardship, I found a prestigious job. I note the conceit arising. I try to take it as part of the Eightfold Path: just an honest Right Livelihood and honestly sharing merit. Any other tip?
53:06 When noting sitting posture, between each note, there should be an awareness of the posture. Does this require looking at the posture and seeing before noting, each time we note? As if keeping-track?
53:42 It seems to me that I never grasp the emotions, as after once I notice them, I can only see the physical sensations clearly. How can I learn about the emotions if I can't see the mental part?
55:42 How to note the transition from sitting to walking meditation?
56:20 Sometimes while meditating, I feel like a wet sock (keep getting distracted, and also it stops me from wanting to go to the gym). This makes me feel lazy, and I feel bad because it's a sloth/torpor hindrance. Please help.
57:21 I sometimes feel quite exhausted after continuously meditating for a while, and I don't feel like getting back to that intense focus. Any solution to that?
58:33 Sadhu...
