00:00 Welcome
04:03 On the topic of The Three Jewels
28:54 Does one need a certain level of intelligence to understand Buddhism and meditation?
31:39 Regularly noting the touching points seems impossible. I almost always get distracted before noting "sitting". There seems to be so many sensations that arise, and if it's not one thing, it's another. I've noticed when I note the frustration or the seemingly overarching feeling that I'll never have a smooth meditation, I feel a sense of relief, like the noting worked — like the noting and the meditation finally worked. Am I onto something with noting what feels like overarching feelings? Even if it feels like I have to sort of look for it?
33:43 When meditating, sometimes I see a flash of an image, perhaps from a movie or a memory. Other times there is a long stream of conversation. Should I note them the same or differently due to duration?
34:27 How should we deal with difficult meditation sessions? I sometimes experience obstacles in my meditation and often end up beating myself up, since I am not sure if I'm doing enough.
35:23 Should we eat in the morning before formal meditation? Isn't it bad for digestion to exercise right after eating? Not sure how true it is, but if it is, does the slowness of walking negate this?
36:35 When meditating, after a thought is complete, I note the thinking. I noticed there are different levels of noting, either stopping and taking my full attention, or sometimes taking my partial attention to note. Which is correct?
37:19 I realized I'd been neglecting the instruction to look 6 feet ahead of you while doing walking meditation. I usually look at my feet. What's the reason for this, and how important is it?
38:28 How can I focus on meditation when I'm in great pain and I get constantly interrupted? Pain I can manage, but I live with people that strongly oppose Buddhism, so I have to practice in secret.
39:47 Sometimes when I don't push myself to note 1 note per second, it feels easier and more efficient. It feels like maybe 1 note per second is too fast for me. Is this okay?
41:20 During meditation, as my mind starts to wander, sometimes I can notice the moment before it wanders off. Should I force it back to the present or simply note it before it goes and once it comes back?
41:53 Is noting "Knowing" or "feeling" vs. something more specific a crutch that should be removed? Sometimes I get a feeling I can't name and call it "knowing" every time.
42:25 How can we balance "practicing as if our heads were on fire" vs. not forcing ourselves/pushing ourselves?
43:00 Is it possible to be deeply immersed in sorrow throughout the meditation without the object? Is it due to expectation?
44:11 What is something you find people commonly misunderstand about meditation?
44:49 I realized that I cannot incorporate anything into myself. Like everything IS outside of myself including myself. Is this non-incorporateness the same as non-self?
46:23 You recommend the noting technique in meditation. However, some teachers see noting as a distraction and recommend only breath or body awareness. Can one reconcile these different views?
47:08 You mentioned that if one dies with an impure state of mind, it is possible to be reborn as a lesser being. Would this still happen even if one maintained a pure mind the majority of their life?
48:08 What are the differences in feeling between each jhana? Is jhana a reasonable path to stream entry or are there other requirements and conditions?
49:43 Do meditation journals help the meditation practice?
50:20 Sadhu...
