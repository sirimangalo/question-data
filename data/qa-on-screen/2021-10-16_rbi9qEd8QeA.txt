00:00 Welcome
00:48 Meditate Together
10:03 Can you please elaborate on the mechanism of how seeing clearly cultivates wisdom? I’m not trying to over-intellectualize, I just feel like I’m missing this part of the technique.
20:11 When my job makes me anxious it is hard to be mindful of this feeling while meditating. I find it unclear and difficult to focus on. Can I help myself see my anxiety more clearly?
21:51 Is there an auditory meditation? Listening to every little detail around me makes me feel present and my thinking goes away.
23:02 When I note an emotion, is it a must to note the exact emotion one feels like “liking or hating etc.?” All this time I’ve just been noting “feeling” without specifying the type of feeling.
24:11 How can I rise above my fear of flying? I am flying from Toronto to British Columbia in November, and I am very nervous. How can I use meditation to overcome this fear?
26:39 I am sometimes oversensitive to the suffering of others, is there an approach I should use for this experience?
27:40 Is something being the "most prominent" conceptual? Technically nothing is the most prominent, because only one thing can be experienced at once, right? Maybe I overthink this.
28:48 Is it possible for an experienced meditator to spend at least 1 minute with no mental noise or thoughts?
29:40 Can I use the note overwhelmed regardless of if I'm upset about the overwhelming feeling or not? Sometimes there's a tinge of disliking, others it feels like I'm overwhelmed in the sense a computer is.
30:43 Is it good to designate notings for certain experiences so you don't have to think about it in the moment? Or is that antithetical to noting things as they strike you or is it too obsessive?
32:40 Can a Sukkhavipassaka (yogi who performs vipassana only) attain the Phala (the fruition)? How is the consciousness process of it?
35:45 How to deal with negative and hurting people?
36:55 I want to be happy. Is this a realistic wish?
38:57 What is the root cause of compulsively seeking mental stimulation?
39:48 How do you know which meditation objects suit you best?
40:56 Since I started practicing this meditation, my confidence regarding intimacy with women has decreased a lot. Is there a way to balance lust and spiritual progress?
42:26 My family tends to trigger huge anxiety and worry in my mind, it gives me enormous pain and confusion as I do love them dearly. How can I work on this issue to help free the mind and heart?
43:31 My mind feels very fuzzy. I sit down to meditate but just feel distracted. I used to have OK mindfulness. Can I ever get back to that state?
45:01 Can a person want something without desiring it (there is no longing for it but rather a preference for it)?
46:15 Should we ever structure practice towards investigating particular fetters? For instance, should a Sotapanna practice differently from an Anagami?
46:54 Since I am on a new medication for schizophrenia, I have problems with meditation. It's harder to see clearly. Should I go back to just old medication?
48:04 What does the Nirvana state actually feel like?
48:19 What is it called to be content with what you have; to not desire more?
48:58 My house has lots of insects. My wife wants me to call pest control and get the house and the surrounding area to be sprayed. I would like to know if this would have a negative impact in terms of karma.
50:29 Do you think the blissful nature of deep conscious presence to be quite intoxicating? And the pursuit of presence through meditation to be addictive?
51:34 Are marital sexual habits positive or hurtful for meditation? Does constant release of sexual energy decrease ability to focus?
53:29 There seem to be parts of my mind that are resistant to sensations arising one after the other during meditation. I feel I can't control that. Is there anything that I should do besides keep noting?
54:39 When the Buddha said something to the effect of "don't concern about the specifics" does that effectively mean noting broad and general?
55:34 If a doctor is asked to end a patient’s life by the patient due to extreme suffering and incurable illness like a cancer, would that be bad karma for the doctor to perform the duty?
59:09 Is "trying" an adequate noting? I.e. trying for happiness, to change experience in some way, etc. Are wanting, disliking, etc. better?
1:00:06 Sadhu...