00:00 Welcome
00:16 Meditation Q&A
20:26 Is it important to have a meditation teacher, or someone to talk to regularly about your practice? I don't have a teacher to talk to, weekly. Should I try to get one?
21:38 I feel isolated. I stay at home, unsure where to go or who to see. Meditation has left me conflicted. I can accept my situation, but feel Iam deprived of a happy life. Should I look outwards or inwards?
23:52 Will Samatha meditation be easier after practicing Vipassana?
24:41 I was meditating 15-20 minutes, but when the meditation gets longer I'm more hesitant to practise and miss some days. However if I go back to 10 minutes, it seems that I won't miss any. What should I do?
26:10 I have emotional flashbacks, childhood trauma memories, but without images. They are extremely intense, but short, like a flash. Too short to see them clearly. How can I deal with them?
28:17 I feel a general animosity towards all people that mindfulness isn't supposed to help get rid of, but I don't want that. Any suggestions?
29:14 How does one deal with the fear of death? Or rather being killed?
30:13 How do you keep your back straight in sitting posture? Is the body relaxed, or are you holding your back in place?
31:15 Since starting the practice, I have lost interest in pursuing material success. But If I don't pursue material success, I feel like my family will be very unhappy. Any advice?
34:08 When we meditate we say rising falling and we focus on our stomach moving, is that all we are doing? Time goes faster doing that, but after I don't feel anything, not happy or unhappy. Is that right?
35:29 I keep falling back into patterns and habits and all, just because I am doing of all this alone - is it wise to find a community? It's hard in my position. I consider joining a temple. What do you think?
36:42 Lately I've been unable to meditate for any length of time. I have been living in chaos. Is there anything I can do to purify myself to better be able to meditate?
38:12 Should the practice of metta bea regular part of one's meditation practice?
38:47 How long is your at-home online meditation course? Can I talk with you weekly without actually doing the course? Just doing my normal daily practice.
39:31 Why is it said that if one meditates regularly, it is highly likely that they will encounter the technique in future lives? Should we take solace in this?
41:06 I'm not a Buddhist; I don't have a religion. Would meditation still help me in my death?
42:46 Can seeing clearly be attained by being mindful of our daily actions and feelings rather than with meditation?
43:57 Is killing any specific living being worse than another? Do specific living beings have more intrinsic worth than others?
47:09 How can one deal with fear of Nibbana?
48:42 Do meditators always have to look solemn, look formal, not allowed to laugh, to be happy, or get excited?
50:39 Can a non-enlightened person have their sense of self completely vanish?
51:38 If Buddhism is about self liberation, it appears inherently selfish. What is the point of being compassionate, loving or caring instead of being self-centered?
55:17 Is it necessary to dissolve negative feelings associated with those past memories, as they can trigger and affect one's present? And since they will show up anyway in the moment of death?
56:55 Is gratefulness a wholesome state?
57:29 Does anger manifest as violent perceptions only? Are there other sankharas to note for anger?
57:55 What is the root cause of the desire for praise and how does one overcome this wish to be thought well of?
59:34 Sadhu...
