00:00 Welcome
01:09 Meditate Together There will be no audio during meditation. Feel free to post questions in the chat.
16:05 I have observed that when I start doing regular practice, I start to wakeup early in the morning to the point that sometimes I wake in the middle of the night. Do you think this is because of the practice, and how to mitigate this?
23:08 How can you work with anxiety during meditation?
26:14 Mahasi Sayadaw prescribes noting "intending" before any bodily movement. Should we adopt this practice?
27:15 How’s does one cultivate concentration with mindfulness?
29:41 Does "sati" in meditation not mean more like memory in background like in a context, not fixing on momentary experience?
31:28 In sitting, I correct my posture about 10-20 times in 30 minutes. I note these movements as "adjusting". Should I do anything else, like strive to limit these movements or experiment with cushions?
32:26 Is it correct to return to the stomach when there is nothing pressing in one's awareness? There is never close to a perfectly single pointed awareness of the stomach in my experience.
33:36 With mantras, is it helpful to be more specific by using a wider range of words to describe what you're feeling/thinking etc or is it best to keep it simply to "liking", "disliking", etc?
34:34 Is there any utility to simply using meditation to relax if you're overwhelmed? (providing one does not get attached to using it as such)?
36:17 I have recently lost my job and have been feeling anxiety over my next steps. Is there a direction or specific kind of job one should aim for as a Buddhist?
39:49 Iam suffering from stress induced increased inflammation in body which is causing swelling in nerves, which meditation can help me in this situation?
42:24 I have no thoughts, feelings, or sensations, but experience pure unimpeded awareness of emptiness and rest in it. What should I note and why? I find silence is perfect as it is, noting was mind training.
45:27 Is it okay to smoke nicotine free vapes just for aesthetics and cool smoke effects? (it has no intoxication or side effect)
47:13 I meditated almost everyday consistently for about 2 years. However, just recently I have not meditated consistently or practiced formal for days at a time, but stay mindful. Should I worry about this?
49:58 Is having casual sexual intercourse with different people considered breaking the sexual misconduct precept? And are energy exchanges of the people engaging in the act a real thing or concept?
52:07 When one lives around people who are still attached, how can one avoid being exploited or asked too much of?
53:46 Do you have any advice for how to find a balance between being careless about the world's problems and being overly concerned with worldly problems?
54:56 When I drive should I note "driving" or "seeing"?
55:47 I was in relationship with a girl but my relationship ended as I was not good enough, I feel deep regret when I try to meditate or concentrate, I get into same regret. How I can get over it?
57:15 Is joking considered breaking the 'do not lie' precept? Yesterday I made up a joke and told my brother to see his reaction but I was self conscious and worried if this is right, we both had a laugh.
58:36 Can we establish a relation between Buddhism and modern medicine? Do they have more differences or similarities?
1:00:32 Sadhu...
