00:00 Welcome
01:21 Meditate Together
15:35 I've become very apathetic and tired of my ambitions. Can you please advise on how to deal with disenchantment of worldly pleasures?
19:19 While meditating this week, I started getting waves of euphoria throughout my whole body that lasted about three hours. It felt good, but don't understand why it happened. Do you know what causes this?
22:09 My mind is often focused on the mantra rather than the experience. Instead of my mind being at the stomach, it is actually with the mantra. Is this a sign of wrong practice?
24:15 Has the statement "we have no control" an exception in the present moment? Otherwise, how would it make sense to say "try to see clearly!"? Or in what way can you try something without control?
26:04 Sometimes while I'm meditating, I fall asleep, but somehow I keep meditating while dreaming. I say to myself, "dreaming". Is there somewhere I can read serious texts about this?
26:59 How do we approach thoughts that cause us to react with fear or anxiety?
28:15 There is a colleague at work who saps my positive energy. I'm finding it hard using this as a lesson. Can you offer any advice?
32:02 What about practicing mindfulness as you have taught, but not using any labeling, mantra whilst doing so? So when I'm walking, I'm only aware of that I'm walking, but not thinking "walking" "walking"
35:42 Is there such thing as "the dark night of the soul" in Buddhism? I hear a lot of people talking about this on various forums and it's putting many people off meditation.
38:43 Is noting a form of thought construction, and does this lead to becoming and rebirth because the meditator is trying to control the experience by labeling it, rather than just watching?
43:21 Can you perhaps explain more about the Jhanas? How can one really know that one has entered Jhana stages?
45:15 Is taking valerian root (often used as an herbal sleep aid) breaking the fifth precept?
48:07 How does the virtue gained from meditation compare with virtue from non-meditative practice like Five Precepts? Weren't non-meditative practices responsible for instant realization during time of Buddha?
51:41 Is it normal along the path of a non-practitioner to break the Five Precepts? I try, but I fail to keep them.
54:28 Sadhu...
