00:00 Welcome
01:37 Meditate Together
16:07 When I'm trying to note my anxiety even without disliking or avoiding it, the physical sensation caused due to the anxiety overcomes the act of noting, because of which I'm unable to see clearly. May I have some advice?
18:11 During practice, I note I haven't been on the foot or the stomach for long time. How do I reduce the moment of wandering during meditation?
19:28 All teachings say: "See the truth of things and thoughts as it is". By teachings, I 'know' the truth, but its hard for me to 'see' & realize the true nature through meditation. I only note but cannot see. How I can develop the insight to see?
23:53 I've been meditating and being mindful more and more. I'm facing challenges with adhd and not being able to take actions. I'd like to do this without meds. Can you comment or advise, please?
26:19 I often struggle to keep my back straight during sitting meditation so I lean against the wall with a pillow. Is this okay? Do you have recommendations for keeping the back straight?
27:35 Is it unwholesome to read and think about Nibbana? Similarly to reading about the 16 stages of knowledge before starting to practice.
29:27 Is noting enough when dealing with more serious hinderances or should they be actively removed?
31:21 How does one determine if friendships are worth cultivating? Since meditating regularly I notice that my friends cause me to be tired and not be mindful. Should I let go of these friendships?
33:31 I am taking a suplement that helps with schizophrenia, like I can work now and am more active, but I have a problem seeing arising and ceasing of experience. What can I do? I want to stay on it...
36:53 What is the skillfull way to respond to suffering that doesn't end and prevents you from doing positive actions? Grief/loss of mother.. having no one to talk to.. bad actions in past.. etc.
42:29 Sometimes when I meditate, I get a sudden jolt- an energy shock- the feeling I've been driving and waking up to a car crash. I continue to meditate; it's something like a big release. I'm not sleepy. It doesn't happen every time, but over 2 years of meditation it still comes up. Why is this? Is it just the energy system. Should I be mindful when it happens and ends and go deeper?
44:45 Are there any practical tips through meditation to let go of people who still live? For example: people who let go of us or hurt us?
47:37 My last meditation session ended in an extreme experience. I've seen a lot, and felt a lot, but it was scary. I haven't meditated since; it's been two years. How do i get back to myself?
50:00 How can the practice help alleviate symptoms of depersonalization / derealization?
52:26 What is a good metric to tell whether the meditation was productive after meditation?
54:17 How can I tame my mind?
54:58 How can an advanced meditator be indifferent and still make choices? Doesn't choosing one option over another imply/require partiality?
56:46 Any advice on dealing with the staggering and sometimes insurmountable student loan debts being collected these days?
1:02:53 Sadhu...
