00:00 Meditation Q&A
04:09 Meditate Together
15:13 How can I note thoughts that lead me to more confusion during the meditation and correct them later or use wisdom to learn from them? If I just keep noting, do I not get any new insights for myself?
19:06 How do I get rid of lust? The hardest attatchment for me to get rid of is sex. I'm trying to be celibate, but it's very hard because of how over sexualised society is and societal pressure, et cetera.
26:53 Sometimes while working, I get distracted by the hindrance of dullness. I note the tiredness but it takes over and sometimes I’m forced to listen to music to gain energy. How do I deal with this?
28:57 I find the thought of meditation boring. I understand the benefits but the resistance behind the thought of its boringness often prevents me from practicing on my own. How can I overcome this?
31:50 Sometimes I get thoughts about important stuff from daily life and I make decisions during meditation and they are really important. Even if I note, I go back to making decisions. What should I do?
35:16 In meditation sometimes I am angry, and then angry that I am angry. How can one let go or overcome anger or sadness like this? By seeing the three characteristics? When wanting something, is it important why?
40:59 How can I let go of obsessive thinking about a person who rejected me? I feel addicted and my energy goes there during meditation. How can I label it?
44:35 If not in the mind, where do we note the rising and falling of the breath?
45:13 All I can do is 10 minutes per day (5 walk, 5 sit). I made a resolution of it and I am sticking to this to build the habit. I try to never skip, but it's still tough many times. Is it OK for the moment?
49:52 Should we practice reflection about Buddha, Dhamma, and Sangha. Is it helpful?
52:38 I've communicated some thoughtless, stress-induced thoughts to some business partners and I fear it has affected their respect for me. I feel ashamed. How can I overcome this?
55:50 Sometimes I get very tired and it becomes hard to do sitting meditation. Should I switch to lying meditation or just label ‘tired’ and is there any way to avoid sleeping during lying meditation?
56:52 Is there a concept of an eternal self in Buddhism? Or is that impermanent like everything else?
58:04 I might get invited in front of a class of teenage pupils to introduce Buddhism. What could I talk about to make this fruitful? I am familiar with buddhist concepts and have done the at home course.
1:00:04 Sadhu...
