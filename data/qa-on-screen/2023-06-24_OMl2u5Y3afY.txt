00:00 Welcome
01:24 Meditate Together
15:57 When we note/label, should we make sure to enunciate the word fully in our mind, or is it OK to mumble it or think the start of the word only, if that's what happens naturally? Sometimes it's almost not even a word, but just a sound, unless I make effort to enunciate.
17:26 How does one avoid getting stuck in one of the levels or states of meditative absorption (Jhana)? How does it happen in the first place? Is it something to be aware of and prepare for?
21:04 I'm a student but I really want to keep up with Dhamma practice. Is there a way to integrate Dhamma in my student life? What intentions should I have while studying? Kindly suggest some Dhamma books.
23:08 What advantage(s) do you see in practicing Satipatthana Vipassana for the lay person after they are far along the path of enlightenment?
24:19 What should I do if after initial concentration on the breath/abdomen I start feeling anxiety and dislike of anxiety? I then focus on that feeling, but that lasts until the end of the meditation.
27:13 Should I meditate right before bedtime or a few hours before?
28:38 Can mindfulness breathing meditation alone lead someone to enlightenment?
29:38 Is practicing wholesome states (love and kindness meditation) beneficial? I've asked myself this question. I see it as just feelings like any other, and this practice would lead to clinging to them.
30:46 I lowered my anti-psychotic medication to half...l don't have psychotic symptoms, but I almost completely stopped this meditation. Before I did up to 4 hours a day. What can I do? It seems overwhelming.
33:23 If Samma-Samadhi is synonymous with Jhana, will it occur spontaneously inside our labeling practice?
38:34 Sometimes I feel frozen in meditation, like my mouth opens involuntarily and I start breathing in through the mouth. It's hard for me to open my eyes afterwards. May I have some advice?
40:01 Is it necessary to note sensations using words, or is it more about noticing/being aware of when the sensations happen? (e.g. I notice sensations when they happen and only note when I'm overwhelmed)
41:22 When eating and listening to Dhamma simultaneously, which should I note? If I note one, I can only focus on one, can I have an open awareness on both? Can we bypass noting in this case (multi-tasking)?
42:45 Are there any situations in which you would recommend tranquility meditation? If so, would it be appropriate to practice it solely by noting the rising and falling of the abdomen, without labeling the overwhelming painful situation?
45:04 What is rebirth and what is reincarnation?
46:15 Is it wholesome to sacrifice your life if it is to save another being? For example saving a drowning man knowing doing it you'll die. Or can it still be uUnwholesome depending on why you save a being?
50:02 I have been Christian for a long time. I meditate a few times a week. What is your take on doctrinal issues between religions, which sometimes seem incompatible?
52:19 Should one try to avoid activities that make anger arise? I love doing sports but the competitive nature brings out the worst in me.
53:38 Does it go too far to say that all of conceptual reality is an illusion or trick of the mind?
54:50 After a long day of mental work I am very tired. You recommend noting "tired" and say it goes away. It doesn't. Mind and body (brain) are inter-dependent, how can noting overpower physical effects?
57:01 When I note, sometimes mental images arise in my mind (ex: if I note anger, a cloud with the label "anger" written on it arises). Basically, all I note comes with a supporting image to support it. As seeing it makes it easier to note or recognize, is this the right practice as I consciously construct these images, or should I note them too and stop encouraging their rising?
58:40 I am struggling with the words of Thanissaro Bhikkhu that said that Samsara is a sick joke. Even in death we can't escape. How do I note this?
1:01:22 Sadhu...
