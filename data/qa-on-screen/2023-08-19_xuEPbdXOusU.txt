00:00 Welcome
01:14 Meditate Together
18:12 I'm diagnosed with ADHD and often I have trouble physically getting started with things. I know what I should do but my body won't move. I try to note everything I feel, but it seems to mostly be bodily sensations/wanting/frustration and I remain stuck for long periods. Is this correct practice/the best thing I can do?
20:56 Sometimes my mind is disturbed by stories that keep on distracting me. What is the best way to work with that? Reflect on "stories, stories’, or "wanting wanting", or "anxious anxious’...?
22:38 I have schizophrenia and sometimes I use my ego rising in my chest to feel better and overcome paranoia. I'm trying to stop doing this and heal myself the right way. Is it possible, or is my search just in vain?
26:11 How do we deal with darkness or murkiness? Do we simply note it while it’s there? When it arises? Is there a protective practice against these? Or a routine antidote (such as metta for anger)?
27:48 Since I started practicing Vipassana, I observe my behaviour closely. When in social circles, I tend to subtly boast about myself, and I feel a superiority complex. However as soon as I am alone, I regret everything that I said. How do I control my ego?
29:33 You have said before that the arising of the word ‘rising, or ‘falling’ has to be after the fact of rising the stomach or falling the stomach, but I feel that both things happened simultaneously. Can you please help clarify this?
31:57 Sometimes I can't pinpoint exactly what is happening and my noting is imprecise. I use my intellect and thoughts to ponder it until I figure out the most precise term. Is this proper?
33:09 How important is the guru or teacher on the Buddhist path in accordance with the Theravada tradition?
36:13 In mindfulness training, what are some of the signs that show that progress has been made?
38:14 Do you have advice for certain OCD-type tendencies when noting, like making a note then judging self for making an "incorrect note", noting the judgement, feeling uncertain and these loop cycles?
41:28 In our everyday life after meditation, should we continue focusing on the sensations of the breath or should we focus more on the sensation of the body, such as "hand, feet, tongue, face"?
42:43 My understanding is that a Buddha (at least in some circumstances) can predict whether and when someone becomes a Buddha or Arahant. Can this foresight apply universally or are there limits?
43:11 I am seekng advice for dealing with lust in the long term. Sexual desire is built into us and I see it as defilement, but many times it feels like torture. Is it more conducive to follow the Buddha and “get it out of my system” until the age of 29? Don’t we need more experience to know its delusion?
46:13 Could you explain 'sampajanna’, 'vitakka', and ‘vicara’' from a practical perspective during meditation?
49:13 What is the main text to follow for a Theravada Buddhist?
50:16 Sometimes noting feels like an obstacle to entering samadhi (8th factor). Can you explain how noting and samadhi can co-exist?
53:15 Can non-vegetarian food affect our meditation in any way?
53:56 What are the ways to meditate during a state of strong addiction? I tried to do mantra meditation that you taught and noted lust and body but these unwholesome states are overpowering.
55:25 Critics of the Mahasi method say it's limited and can only take you to Sotapana. Due to latency of noting-experiencing, it must be abandoned after clear mindfulness is established. Can you refute this?
58:51 Sadhu...
