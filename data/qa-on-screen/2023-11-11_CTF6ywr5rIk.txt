00:00 Welcome
02:00 Meditate Together
13:36 If I suppress my personal problems and communicate with others through smiles, could this behaviour be called into question regarding honesty?
15:45 I'm adopted from Sri Lanka to Denmark and I don't know my biological parents. According to Theravada Buddhism, do I then have an obligation to find and locate them even though that's a painful and difficult process? Or are my obligations only towards my adoptive parents? I'm careful not to make a narrative out of this and also to be mindful of the thoughts, feelings and reactions that arise.
18:40 During my sitting meditation, I am usually not able to be aware of the touching points. Sometimes I do feel a subtle sensation, but again I am not sure whether it's real or imagination. Do you have any advice?
20:45 My greatest hindrance in meditation is the mental furniture left over from my previous faith. Prayer and pursuing union with God emotionally feels right as opposed to being mindful.
23:22 Do actions trigger certain mindstates? I had been trying to build better habits and saw good effects, then habitually/foolishly added just a few old habits back and old mindstates appeared.
25:06 When I'm mindful, I feel like I'm a disciple of the Buddha, of Master Yuttadhammo. I'm safe, righteous, and worthy. When I'm NOT mindful, I feel like I'm none of the above. Is it correct?
27:46 After cultivating the habit of mindfulness, I see random states of meditation/mindfulness occur on their own without any effort. Does the mindstate's experience become a habit of mind, and thus more effortless?
29:10 Are these claims correct? 1. We should not try to control experience; it's not possible. 2. We should try to change our perspective through meditation (let seeing be seeing); this is in contrast possible.
30:14 Do you have any advice for self-sabotage? It is frustrating to fall into same bad patterns (mental habits) again and again.
32:01 Since practicing meditation, I’m seeing thoughts arise on their own. Is it a good habit to cultivate new good thoughts for future arising, or will my actions do this alone?
33:04 When doing walking meditation, during the standing portion: how exactly do I focus on the standing? Should I focus on my entire body, the standing posture, or something else?
34:00 At times when I try to meditate, my mind is overwhelmed with thoughts that are all over the place, which makes it difficult to sit still and focus on “noting”. Should this get better with practice?
34:58 In walking meditation, I don't see anymore the arising and ceasing of stepping, and it feels like I'm walking meaninglessly.. what should I do?
37:02 When we return to the abdomen after we note a distraction, why do we wait for the rising to start again instead of noting whatever the movement is, such as if it was mid-rising or mid-falling?
38:06 What is the right way of understanding of the three marks of existence (other than the present moment seeing)? When I think and see things being impermanent and unsatisfying, by default a sense ofdespair and sadness comes with that understanding.
40:34 I have ADHD and struggle to meditate. Do you have any advice for making this easier?
43:53 Is it possible to meditate while studying for exams? If possible, how can I do this?
46:35 Is it beneficial to intentionally think distressing thoughts during meditation as a means to strengthen the mind?
50:11 I will attend 10-day Vipassana course (Goenka) to meditate. I already have a spiritual path and I will incorporate my learning. Any advice, especially on how to continue to effectively meditate?
57:31 Is it all right to pray to Devas for assistance?
1:01:55 I feel like a loser in life. I have no wife, no kids, I'm broke, and I'm overweight. How can I use meditation to address that?
1:12:24 I find it very difficult to observe thoughts during meditation, because as soon as I realize that I was lost in thoughts, my thoughts disappear and there is no more thought in my head that I can observe. Do you have any advice?
