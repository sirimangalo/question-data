
# Video Chapters Recognition

Utility to recognize the change of text displayed in a video like https://www.youtube.com/watch?v=EWVHkoC4MUg. The algorithm reads the text from the video and extracts the "switching" times. The helper script then creates a list of chapters with start time and corresponding text, which after potential manual editing can be used as chapters in the YouTube video description.

## Dependencies

- python3
- cv2 (OpenCV)
- pytesseract
- editdistance

`pip3 install --user pytesseract editdistance`

## Usage

The script `recognize.py` expects the file of a video (i.e. downloaded with `youtube-dl`).

```bash
./extract-chapters.py <videofile> # This might take a while
```

It will create a `<filename>.chapters.json` and `<filename>.chapters.json.txt`. The latter one contains the formatted chapters, but still needs manual oversight since there could be conflicts marked with `[CONFLICT] --->` in case two chapters are less than 10 seconds distanced to each other.

### Example Output

In `<filename>.chapters.txt`:

```
Video: https://youtu.be/9hN7Sg.f137

-------------------------
00:00 Welcome
05:11 Could you talk more on mindful walking?
05:57 What's the benefit of noting stepping right or left? Aren't right and left concepts?
08:26 Since noting is an acknowledgement of what happens, should it always follow the event being noted, or should we try to align it with the event as it occurs?
09:10 Am I performing a bad karma if my family is suffering mentally because of me, as they don’t want me to practice the Dhamma? They have certain fears and attachments which make them think so.
11:55 When doing walking meditation is it OK to have attention on the sensations In the leg, or should one maintain attention on the foot?
12:27 I feel anxious when I meditate, been falling off the spiritual path and have lost interest. How do I regain that passion that I had when I first WOKE up and awakened?
14:27 How to be mindful about listening? what should we focus on, specifically?
15:29 Right now I am able to keep the 5 precepts and practice with quality for barely 10 minutes walking and 20 minutes sitting everyday. Do I have to push to 30-30 to even just start the at-home course?
16:27 How to deal with backpain while meditating? Especially after a longer time, after one hour for example
17:35 Is it possible to run away from meditation when first new to it, like when it gets hard, challenging things come up??
```

### Known Issues

- Text recognition errors
- Same chapter repeats itself because of errors in text recognition
- Fairly long runtime
