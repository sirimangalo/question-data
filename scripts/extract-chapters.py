#!/usr/bin/env python3

import cv2
import re
import json
import pytesseract
import argparse
import editdistance
from datetime import timedelta
from os import path

parser = argparse.ArgumentParser()

parser.add_argument('--debug', action='store_true', help='Show debugging information.')
parser.add_argument('videofile', help='File of video to analyze.')

args = parser.parse_args()

def get_text_at_second(cap, t):
  """
    Recognize text of frame at a certain time.
  """

  try:
    cap.set(cv2.CAP_PROP_POS_MSEC, 1000 * t)
    ret, frame = cap.read()
    img_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return pytesseract.image_to_string(img_rgb)
  except:
    return 'Recognition Failed!'

def is_same_noisy_text(a, b):
  """
    Check text equality, but allow for a certain
    amount of differences due to noise.
  """

  min_len = min(len(a), len(b))

  if min_len <= 3:
    return a.lower() == b.lower()
  elif min_len <= 5:
    return editdistance.distance(a.lower(), b.lower()) <= 1
  elif min_len <= 10:
    return editdistance.distance(a.lower(), b.lower()) <= 3
  elif min_len <= 20:
    return editdistance.distance(a.lower(), b.lower()) <= 6
  elif min_len <= 30:
    return editdistance.distance(a.lower(), b.lower()) <= 10
  elif min_len <= 40:
    return editdistance.distance(a.lower(), b.lower()) <= 15
  else:
    return editdistance.distance(a.lower(), b.lower()) <= 18

def clean_text(text):
  cleaned = text
  cleaned = text.replace('\n', ' ')
  cleaned = re.sub(r'\s+', ' ', cleaned)
  cleaned = cleaned.strip()

  cleaned = cleaned.replace('For more on meditation in our tradition, please visit: https://sirimangalo.org/', '')
  cleaned = re.sub(r'^Question', '', cleaned, re.I)
  cleaned = re.sub(r'sirimangalo\.org$', '', cleaned, re.I)

  cleaned = cleaned.replace('|', 'I')

  return cleaned.strip()

def format_pos(secs):
  return re.sub(r'^0\:', '', str(timedelta(seconds=secs)))

def format_chapters(json_file):
    with open(json_file) as f:
        texts = json.loads(f.read())

    res = ''
    t_before = -10
    for t, text in texts:
      if (t - t_before) < 10:
        res += '[CONFLICT] --->' # mark a conflict
      res += '%s %s\n' % (format_pos(t), clean_text(text))
      t_before = t

    return res

def process(file):
  """
    Extract chapters with text from video file
  """

  cap = cv2.VideoCapture(file)

  # get duration
  fps = cap.get(cv2.CAP_PROP_FPS) # https://stackoverflow.com/a/52032374
  frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
  duration = frame_count / fps


  # get first frame text
  current_text = ''
  current_text = get_text_at_second(cap, 0)
  current_text_start_pos = 0

  texts = [(0, current_text)]

  t = 1
  step = 15

  while t < duration:
    if args.debug:
      print('Seek', t)

    text = get_text_at_second(cap, t)

    # compare with current chapter text
    if is_same_noisy_text(text, current_text):
      # increase time with a step size > 0
      # and continue to check that time.
      t += step
      continue


    # the current time seems to be a different chapter;
    # -> go back in time until frame text matches current chapter again.
    found = False
    last_temp_text = text
    for j in range(t - 1, current_text_start_pos - 1, -1):
      temp_text = get_text_at_second(cap, j)

      if args.debug:
        print('Backtrack', j)

      if is_same_noisy_text(temp_text, current_text):
        # detected the start of a new chapter
        if args.debug:
          print('New Chapter', j)

        texts.append((j, last_temp_text))
        current_text = last_temp_text
        current_text_start_pos = j + 1
        t = j + 2
        found = True
        break

      last_temp_text = temp_text

    # make sure current chapter was found
    # this assertion should always hold
    assert found == True

  # check remaining frames individually
  for i in range(t, int(duration)):
    temp_text = get_text_at_second(i)

    if not is_same_noisy_text(temp_text, current_text):
      texts.append((i, temp_text))
      current_text = temp_text

  return texts


if __name__ == '__main__':
  try:
    print('Processing', args.videofile)

    texts = process(args.videofile)
    json_file = args.videofile + '.chapters.json'

    # store original capture as json file as backup
    with open(json_file, 'w') as f:
      f.write(json.dumps(texts))

    formatted = format_chapters(json_file)

    with open(json_file + '.txt', 'w') as f:
      f.write(formatted)

    print(formatted)
  except Exception as e:
    print('ERROR', e)
